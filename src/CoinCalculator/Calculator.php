<?php
namespace CoinCalculator;

class Calculator {

    static public $DENOMINATIONS = array(50, 20, 10, 5, 2, 1);
    static public $POUND = '£';
    static public $PENNY = 'p';

    public function calculate($input)
    {
        $prepared = $this->prepareAmount($input);

        if(empty($prepared))
            throw new Exception('Something goes wrong!');

        $endResult = '';

        if(isset($prepared[0]))
            $endResult .= $this->calculateFraction($prepared[0], self::$POUND);

        if(isset($prepared[1]))
            $endResult .= $this->calculateFraction($prepared[1], self::$PENNY);

        return $endResult;

    }

    public function calculateFraction($amount, $currency) {

        $coins = $this->change($amount);
        $result = '';

        $coins = array_count_values($coins);

        $comma = sizeof($coins) > 1 ? ', ' : '';

        foreach ($coins as $coin => $occurence) {

            if (self::$POUND == $currency) {
                if ($coin > 2) {
                    $result .= $occurence . ' * ' . $currency . $coin . ' note';

                } else {
                    $result .= $comma . $occurence . ' * ' . $currency . $coin . ' coin';
                }
            } else {
                $result .= $comma . $occurence . ' * ' . $coin . $currency . ' coin';
            }

        }

        return $result;

    }

    public function change($amount) {

        $coins = array();

       for ($i=0; $i<sizeof(self::$DENOMINATIONS); $i++) {

           $tmp = $amount % self::$DENOMINATIONS[$i];

           if ($tmp == $amount) {
               continue;
           } else if($tmp == 0) {

               if ($amount == 0)
                   break;

               if ($amount == end($coins))
                   array_push($coins, $amount);
               else {
                   array_push($coins, self::$DENOMINATIONS[$i]);
               }

           } else {
               array_push($coins, self::$DENOMINATIONS[$i]);
           }

           $amount -= end($coins);
       }

       return $coins;

    }

    public function prepareAmount($input) {

        $purify = preg_replace("/[^0-9.]/", "",$input);

        return explode(".", $purify);

    }
}
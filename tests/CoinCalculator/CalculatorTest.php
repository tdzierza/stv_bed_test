<?php

namespace CoinCalculator\Tests;
use CoinCalculator\Calculator;

class CalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Calculator
     */
    protected $calculator;

    public function setUp()
    {
        $this->calculator = new Calculator();
    }

    public function testUsingInt()
    {
        $output = $this->calculator->calculate(23);
        $this->assertEquals("1 * £20 note, 1 * £2 coin, 1 * £1 coin", $output);
    }

    public function testUsingString()
    {
        $output = $this->calculator->calculate("23");
        $this->assertEquals("1 * £20 note, 1 * £2 coin, 1 * £1 coin", $output);
    }

    public function testWithPrefix()
    {
        $output = $this->calculator->calculate("£23");
        $this->assertEquals("1 * £20 note, 1 * £2 coin, 1 * £1 coin", $output);
    }

    public function testWithSuffix()
    {
        $output = $this->calculator->calculate("£23.00p");
        $this->assertEquals("1 * £20 note, 1 * £2 coin, 1 * £1 coin", $output);
    }

    public function testMultipleNotes()
    {
        $output = $this->calculator->calculate("40");
        $this->assertEquals("2 * £20 note", $output);
    }

    public function testWithFractions()
    {
        $output = $this->calculator->calculate("20.13");
        $this->assertEquals("1 * £20 note, 1 * 10p coin, 1 * 2p coin, 1 * 1p coin", $output);
    }

    public function testLessThanOne()
    {
        $output = $this->calculator->calculate("0.10");
        $this->assertEquals("1 * 10p coin", $output);
    }
}
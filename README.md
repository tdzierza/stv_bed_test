# STV Back-End Developer Test

This repo contains a test for prospective back-end developers. It consists of a PHPUnit test which exercises a class called `Calculator`, contained within the `src/CoinCalculator` directory. This class has one method, `calculate`, which currently returns an empty string. The objective of the test is to implement the functionality described in the test (at `tests/CoinCalculator/CalculatorTest.php`) so that the tests pass.

This is supposed to mimic a customer transaction of giving change in a shop. The calucate method should accept an input (of mixed type), parse that input, and decide the most appropriate change to give (in GBP). For example, if '£33.83p' was the input, the output should be '1 * £20 note, 1 * £10 note, 1 * £2 coin, 1 * £1 coin, 1 * 50p coin, 1 * 20p coin, 1 * 10p coin, 1 * 2p coin, 1 * 1p coin'.

No web server is required. All that is needed is to check out this repo on a machine with PHP 5.3 or above, run `composer install`, then run `./vendor/bin/phpunit` repeatedly to see if the tests pass.

The denominations for change are:

* £50 note
* £20 note
* £10 note
* £5 note
* £2 coin
* £1 coin
* 50p coin
* 20p coin
* 10p coin
* 5p coin
* 2p coin
* 1p coin




## Submission

For submission, can you provide us with a link to your completed code in a public git repository (on github, bitbucket, or similar services).

Please email this link to HR, and they will send it to our development team for review.